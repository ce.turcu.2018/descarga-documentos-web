#!/usr/bin/python3

import urllib.request


class Robot:

    # Inicialización de las variables de la clase Robot
    def __init__(self, url):
        self.url = url  # Esta es la variable de nuestro objeto que contiene la url
        self.retrieved = False  # Esta es la variable de nuestro objeto que identifica si se ha descargado anteriormente
        print(self.url)

    def retrieve(self):
        if self.retrieved == False:  # en caso de que el documento no se haya descargado antes...
            print("Descargando url...")
            f = urllib.request.urlopen(self.url)  # abrimos la url y la guardamos en una variable local
            self.content = f.read().decode('utf-8')  # leemos el contenido de la url y lo guardamos en
                                                     # el atributo content del objeto
            self.retrieved = True  # cambiamos el atributo retrieved a TRUE para indicar que ya ha sido descargado

    # Contenido descargado de la url
    def content(self):
        self.retrieve()
        return self.content

    # Imprimir el contenido de la url, es decir, del documento Web
    def show(self):
        print(self.content())


class Cache:

    # Inicialización de las variables de la Clase Cache
    def __init__(self):
        self.cache = {}  # Diccionario vacío, donde guardaremos las url que queramos descargar

    # Método para guardar las URLs proporcionadas en la caché
    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url=url)
            self.cache[url] = robot
        else:
            print("el documento ya ha sido descargado")

    # Método para devolver el contenido de la URL almacenada en la caché
    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    # Método para mostrar el contenido de la URL almacenada en la caché
    def show(self, url):
        print(self.content(url))

    # Método para mostrar todas las URLs almacenadas en la caché
    def show_all(self):
        for url in self.cache:
            print(url)

if __name__ == '__main__':
    print("Test Robot class")
    r1 = Robot('http://gsyc.urjc.es/')
    r2 = Robot('https://www.python.org/downloads/')

    r1.show()
    r1.retrieve()
    r1.retrieve()  # Como vemos, el mensaje aparece una sola vez, sólo descargamos 1 vez
    r2.show()
    r2.retrieve()


    print("Test Cache class")
    c1 = Cache()
    c2 = Cache()

    c1.retrieve('http://gsyc.urjc.es/')
    c2.retrieve('https://www.python.org/downloads/')
    c1.show('https://www.aulavirtual.urjc.es')
    c2.show('https://www.python.org/downloads/')
    c1.show_all()
    c2.show_all()
